module gitee.com/kirile/kapi

go 1.16

require (
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0
	github.com/linxlib/conv v0.0.0-20200419055849-46faf16ac98f
	github.com/linxlib/logs v0.0.7
)
